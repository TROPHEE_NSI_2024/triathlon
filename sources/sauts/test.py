import pygame
# from pygame.locals import *

pygame.init()

#Ouverture de la fenêtre Pygame
fenetre = pygame.display.set_mode((1040, 880))

#Chargement et collage du fond
fond = pygame.image.load("piste.jpg").convert()
fenetre.blit(fond, (0,0))

#Chargement et collage du personnage
perso = pygame.image.load("coureur.png").convert_alpha()
perso_x = 925
perso_y = 150
fenetre.blit(perso, (perso_x, perso_y))

obs1 = pygame.image.load("haie.png").convert_alpha()
obs1_x =600
obs1_y = 125
fenetre.blit(obs1, (obs1_x, obs1_y))
hobs1 = 75
lobs1 = 130


obs2 = pygame.image.load("haie.png").convert_alpha()
obs2_x = 200
obs2_y = 125
fenetre.blit(obs2, (obs2_x, obs2_y))
hobs2 = 75
lobs2 = 130


obs3 = pygame.image.load("haie.png").convert_alpha()
obs3_x = 400
obs3_y = 245
fenetre.blit(obs3, (obs3_x, obs3_y))
hobs3 = 75
lobs3 = 130


obs4 = pygame.image.load("haie.png").convert_alpha()
obs4_x = 110
obs4_y = 350
fenetre.blit(obs4, (obs4_x, obs4_y))
hobs4 = 75
lobs4 = 130

obs5 = pygame.image.load("haie.png").convert_alpha()
obs5_x = 700
obs5_y = 250
fenetre.blit(obs5, (obs5_x, obs5_y))
hobs5 = 75
lobs5 = 130

obs6 = pygame.image.load("haie.png").convert_alpha()
obs6_x = 100
obs6_y = 575
fenetre.blit(obs6, (obs6_x, obs6_y))
hobs6 = 75
lobs6 = 130

obs7 = pygame.image.load("haie.png").convert_alpha()
obs7_x = 250
obs7_y = 465
fenetre.blit(obs7, (obs7_x, obs7_y))
hobs7 = 75
lobs7 = 130

obs8 = pygame.image.load("haie.png").convert_alpha()
obs8_x = 700
obs8_y = 465
fenetre.blit(obs8, (obs8_x, obs8_y))
hobs8 = 75
lobs8 = 130

obs9 = pygame.image.load("haie.png").convert_alpha()
obs9_x = 700
obs9_y = 680
fenetre.blit(obs9, (obs9_x, obs9_y))
hobs9 = 75
lobs9 = 130

obs10 = pygame.image.load("haie.png").convert_alpha()
obs10_x = 350
obs10_y = 680
fenetre.blit(obs10, (obs10_x, obs10_y))
hobs10 = 75
lobs10 = 130

lig1 = pygame.image.load("ligne.png").convert_alpha()
lig1_x =0
lig1_y = 100
fenetre.blit(lig1, (lig1_x, lig1_y))
lig1 = 75
lig1 = 130

#Rafraîchissement de l'écran
pygame.display.flip()

#liste_obstacle= [(600,125),(500,235),(400,345),(110,565)]
taille = [75,100]
liste_obstacle= [(600,125),(200,125),(400,245),(110,350),(700,250),(100,575),(250,465),(700,465),(700,680)]

def collision_obstacle(x, y, obstacle):
    return obstacle[0] <= x <= obstacle[0] + taille[0] and obstacle[1] <= y <= obstacle[1] + taille[1]

def collision(x,y, liste_obstacle):
    for o in liste_obstacle:
        if collision_obstacle(x, y, o):
            return True
    return False
    

#BOUCLE INFINIE
continuer = True
while continuer:
    for evenement in pygame.event.get(): #Attente des événements
        if evenement.type == pygame.QUIT:
            continuer = False
        if evenement.type == pygame.KEYDOWN:
            if evenement.key == pygame.K_LEFT:
                #On change les coordonnées du perso
                if not collision(perso_x - 42 ,perso_y, liste_obstacle):
                    perso_x = perso_x - 42
            elif evenement.key == pygame.K_UP:
                if not collision(perso_x ,perso_y - 110, liste_obstacle):
                    perso_y = perso_y - 110
            elif evenement.key == pygame.K_DOWN:
                if not collision(perso_x ,perso_y + 110, liste_obstacle):        
                    perso_y = perso_y +110
            #Re-collage
            fenetre.blit(fond, (0,0))
            fenetre.blit(perso, (perso_x, perso_y))
            fenetre.blit(obs1, (obs1_x, obs1_y))
            fenetre.blit(obs2, (obs2_x, obs2_y))
            fenetre.blit(obs3, (obs3_x, obs3_y))
            fenetre.blit(obs4, (obs4_x, obs4_y))
            fenetre.blit(obs5, (obs5_x, obs5_y))
            fenetre.blit(obs6, (obs6_x, obs6_y))
            fenetre.blit(obs7, (obs7_x, obs7_y))
            fenetre.blit(obs8, (obs8_x, obs8_y))
            fenetre.blit(obs9, (obs9_x, obs9_y))
            fenetre.blit(obs10, (obs10_x, obs10_y))
            fenetre.blit(lig1, (lig1_x, lig1_y))
            #Rafraichissement
            pygame.display.flip()
