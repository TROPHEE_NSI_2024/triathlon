#importation du module pygame dans python
import pygame
from pygame.locals import *
pygame.init()

#ouvrir une fenêtre de 1040px X 880px
fenetre = pygame.display.set_mode((1040, 880))

#délimitation des 3 zones où il faut cliquer pour jouer aux 3 sports
def est_natation(x:int, y:int)->bool:
    # retourne vraie si le point de coordonnées (x,y) est dans l'encadré natation
    return 370 < x < 650 and 240 < y < 300

def est_sauts(x:int, y:int)->bool:
    # retourne vraie si le point de coordonnées (x,y) est dans l'encadré saut
    return 370 < x < 650 and 340 < y < 480  

def est_ice(x:int, y:int)->bool:
    # retourne vraie si le point de coordonnées (x,y) est dans l'encadré ice
    return 370 < x < 650 and 540 < y < 630 
                     
#fonction permettant de jouer sur la piste de natation                     
def page_natation() :
    # jeu natation
    #ouverture de la page
    fenetre = pygame.display.set_mode((1040, 880))

    #FOND
    fond = pygame.image.load("natation/piscine.jpg").convert()
    fenetre.blit(fond, (0,0))
    pygame.display.flip()

    #PERSONNAGE
    perso = pygame.image.load("natation/nageuse4.png").convert_alpha()
    perso.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    perso_x = 750
    perso_y = 75#8 = 720#7 = 610#6 = 530#5 = 430  #4 = 350 #3 = 265  #2 = 170 = #1 = 75
    fenetre.blit(perso, (perso_x, perso_y))

    #obstacle
    obstacle = pygame.image.load("natation/bouee.png").convert_alpha()
    obstacle.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle= fenetre.blit(obstacle,(350,365))

    #obstacle2
    obstacle2 = pygame.image.load("natation/bouee.png").convert_alpha()
    obstacle2.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle2= fenetre.blit(obstacle,(589,267))

    #obstacle3
    obstacle3 = pygame.image.load("natation/bouee.png").convert_alpha()
    obstacle3.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle3= fenetre.blit(obstacle3,(120,185))

    #obstacle4
    obstacle4 = pygame.image.load("natation/bouee.png").convert_alpha()
    obstacle4.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle4= fenetre.blit(obstacle4,(120,645))

    #obstacle5
    obstacle5 = pygame.image.load("natation/bouee.png").convert_alpha()
    obstacle5.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle5 = fenetre.blit(obstacle5,(678,458))

    #obstacle6
    obstacle6 = pygame.image.load("natation/bouée3.png").convert_alpha()
    obstacle6.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle6 = fenetre.blit(obstacle6,(675,720))

    #obstacle7
    obstacle7 = pygame.image.load("natation/bouée3.png").convert_alpha()
    obstacle7.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle7 = fenetre.blit(obstacle7,(487,65))

    #obstacle8
    obstacle8 = pygame.image.load("natation/bouée3.png").convert_alpha()
    obstacle8.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    position_obstacle8 = fenetre.blit(obstacle8,(350,525))

    #liste_obstacle
    taille = [75,100] 
    liste_obstacle= [(350,365),(589,267),(120,185),(120,645),(678,458),(675,720),(487,65),(350,525)]
    
    #fonction empêchant le personnage de franchir les obstacles
    def collision_obstacle(x, y, obstacle):
        return obstacle[0] <= x <= obstacle[0] + taille[0] and obstacle[1]-20 <= y <= obstacle[1] + taille[1] 

    def collision(x,y, liste_obstacle):
        for o in liste_obstacle:
            if collision_obstacle(x, y, o):
                return True
        return False
    
    #Rafraîchissement de l'écran            
    continuer = 1
    #boucle infinie
    continuer = True
    while continuer:
        for evenement in pygame.event.get(): # Attente des événements
            if evenement.type == pygame.QUIT:
                continuer = False
            if evenement.type == pygame.KEYDOWN:
                if evenement.key == pygame.K_LEFT:
                    if not collision(perso_x - 45 ,perso_y, liste_obstacle):
                        perso_x = perso_x - 45
                elif evenement.key == pygame.K_UP:
                    if not collision(perso_x ,perso_y - 98, liste_obstacle):
                        perso_y = perso_y - 98
                elif evenement.key == pygame.K_DOWN:
                    if not collision(perso_x ,perso_y + 98, liste_obstacle):        
                        perso_y = perso_y +98
        # Recollage
        fenetre.blit(fond, (0, 0))
        fenetre.blit(perso, (perso_x, perso_y))
        fenetre.blit(obstacle, position_obstacle)  # Affiche la première bouée
        fenetre.blit(obstacle2, position_obstacle2)  # Affiche la deuxième bouée
        fenetre.blit(obstacle3, position_obstacle3)  # Affiche la troisième bouée
        fenetre.blit(obstacle4, position_obstacle4)  # Affiche la quatrième bouée
        fenetre.blit(obstacle5, position_obstacle5)  # Affiche la cinquième bouée
        fenetre.blit(obstacle6, position_obstacle6) #Affiche la sixieme bouée
        fenetre.blit(obstacle7, position_obstacle7) #Affiche la septieme bouée
        fenetre.blit(obstacle8, position_obstacle8) #Affiche la huitième bouée
        # Rafraichissement    
        pygame.display.flip()
        
        
#fonction permettant d'acceder à la page pour jouer sur la priste de course     
def page_sauts() :
    #page saut
    #ouverture de la page 
    fenetre = pygame.display.set_mode((1040, 880))

    #FOND
    fond = pygame.image.load("sauts/piste.jpg").convert()
    fenetre.blit(fond, (0,0))

    #PERSONNAGE
    perso = pygame.image.load("sauts/coureur.png").convert_alpha()
    perso_x = 925
    perso_y = 150
    fenetre.blit(perso, (perso_x, perso_y))

    #OBSTACLEs
    obs1 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs1_x =600
    obs1_y = 125
    fenetre.blit(obs1, (obs1_x, obs1_y))
    hobs1 = 75
    lobs1 = 130


    obs2 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs2_x = 200
    obs2_y = 125
    fenetre.blit(obs2, (obs2_x, obs2_y))
    hobs2 = 75
    lobs2 = 130


    obs3 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs3_x = 400
    obs3_y = 245
    fenetre.blit(obs3, (obs3_x, obs3_y))
    hobs3 = 75
    lobs3 = 130


    obs4 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs4_x = 110
    obs4_y = 350
    fenetre.blit(obs4, (obs4_x, obs4_y))
    hobs4 = 75
    lobs4 = 130

    obs5 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs5_x = 700
    obs5_y = 250
    fenetre.blit(obs5, (obs5_x, obs5_y))
    hobs5 = 75
    lobs5 = 130

    obs6 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs6_x = 100
    obs6_y = 575
    fenetre.blit(obs6, (obs6_x, obs6_y))
    hobs6 = 75
    lobs6 = 130

    obs7 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs7_x = 250
    obs7_y = 465
    fenetre.blit(obs7, (obs7_x, obs7_y))
    hobs7 = 75
    lobs7 = 130

    obs8 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs8_x = 700
    obs8_y = 465
    fenetre.blit(obs8, (obs8_x, obs8_y))
    hobs8 = 75
    lobs8 = 130

    obs9 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs9_x = 700
    obs9_y = 680
    fenetre.blit(obs9, (obs9_x, obs9_y))
    hobs9 = 75
    lobs9 = 130

    obs10 = pygame.image.load("sauts/haie.png").convert_alpha()
    obs10_x = 350
    obs10_y = 680
    fenetre.blit(obs10, (obs10_x, obs10_y))
    hobs10 = 75
    lobs10 = 130


    #Rafraîchissement de l'écran
    pygame.display.flip()

    #liste_obstacle
    taille = [75,100]
    liste_obstacle= [(600,125),(200,125),(400,245),(110,350),(700,250),(100,575),(250,465),(700,465),(700,680),(350,680)]

    #fonctions empêchant le personnage de passer à travers les obstacles 
    def collision_obstacle(x, y, obstacle):
        return obstacle[0] <= x <= obstacle[0] + taille[0] and obstacle[1] <= y <= obstacle[1] + taille[1]

    def collision(x,y, liste_obstacle):
        for o in liste_obstacle:
            if collision_obstacle(x, y, o):
                return True
        return False
    

    #BOUCLE INFINIE
    continuer = True
    while continuer:
        for evenement in pygame.event.get(): #Attente des événements
            if evenement.type == pygame.QUIT:
                continuer = False
            if evenement.type == pygame.KEYDOWN:
                if evenement.key == pygame.K_LEFT:
                    #On change les coordonnées du perso
                    if not collision(perso_x - 42 ,perso_y, liste_obstacle):
                        perso_x = perso_x - 42
                elif evenement.key == pygame.K_UP:
                    if not collision(perso_x ,perso_y - 110, liste_obstacle):
                        perso_y = perso_y - 110
                elif evenement.key == pygame.K_DOWN:
                    if not collision(perso_x ,perso_y + 110, liste_obstacle):        
                        perso_y = perso_y +110
                
                #Re-collage
                fenetre.blit(fond, (0,0))
                fenetre.blit(perso, (perso_x, perso_y))
                fenetre.blit(obs1, (obs1_x, obs1_y))
                fenetre.blit(obs2, (obs2_x, obs2_y))
                fenetre.blit(obs3, (obs3_x, obs3_y))
                fenetre.blit(obs4, (obs4_x, obs4_y))
                fenetre.blit(obs5, (obs5_x, obs5_y))
                fenetre.blit(obs6, (obs6_x, obs6_y))
                fenetre.blit(obs7, (obs7_x, obs7_y))
                fenetre.blit(obs8, (obs8_x, obs8_y))
                fenetre.blit(obs9, (obs9_x, obs9_y))
                fenetre.blit(obs10, (obs10_x, obs10_y))
                #Rafraichissement
                pygame.display.flip()

#fonction permettant d'acceder à la page pour jouer sur la piste de ice cross
def page_ice() :
    
    #Ouverture de la fenêtre
    fenetre = pygame.display.set_mode((1040, 880))

    #FOND
    fond = pygame.image.load("ice cross/pistep.jpg").convert()
    fenetre.blit(fond, (0,0))

    #PERSONNAGE
    perso = pygame.image.load("ice cross/patineuse.png").convert_alpha()
    perso_x = 925
    perso_y = 125
    fenetre.blit(perso, (perso_x, perso_y))

    obs1 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs1_x =600
    obs1_y = 125
    fenetre.blit(obs1, (obs1_x, obs1_y))
    hobs1 = 75
    lobs1 = 100


    obs2 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs2_x = 200
    obs2_y = 125
    fenetre.blit(obs2, (obs2_x, obs2_y))
    hobs2 = 75
    lobs2 = 100


    obs3 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs3_x = 400
    obs3_y = 235
    fenetre.blit(obs3, (obs3_x, obs3_y))
    hobs3 = 75
    lobs3 = 100


    obs4 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs4_x = 110
    obs4_y = 345
    fenetre.blit(obs4, (obs4_x, obs4_y))
    hobs4 = 75
    lobs4 = 100

    obs5 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs5_x = 700
    obs5_y = 235
    fenetre.blit(obs5, (obs5_x, obs5_y))
    hobs5 = 75
    lobs5 = 100

    obs6 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs6_x = 100
    obs6_y = 565
    fenetre.blit(obs6, (obs6_x, obs6_y))
    hobs6 = 75
    lobs6 = 100

    obs7 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs7_x = 250
    obs7_y = 455
    fenetre.blit(obs7, (obs7_x, obs7_y))
    hobs7 = 75
    lobs7 = 100

    obs8 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs8_x = 700
    obs8_y = 455
    fenetre.blit(obs8, (obs8_x, obs8_y))
    hobs8 = 75
    lobs8 = 100

    obs9 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs9_x = 700
    obs9_y = 675
    fenetre.blit(obs9, (obs9_x, obs9_y))
    hobs9 = 75
    lobs9 = 100

    obs10 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs10_x = 350
    obs10_y = 675
    fenetre.blit(obs10, (obs10_x, obs10_y))
    hobs10 = 75
    lobs10 = 100

    obs11 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs11_x = 400
    obs11_y = 15
    fenetre.blit(obs11, (obs11_x, obs11_y))
    hobs11 = 75
    lobs11 = 100

    obs12 = pygame.image.load("ice cross/flaquee.png").convert_alpha()
    obs12_x = 224
    obs12_y = 785
    fenetre.blit(obs12, (obs12_x, obs12_y))
    hobs12 = 75
    lobs12 = 100

    #Rafraîchissement de l'écran
    pygame.display.flip()

    #liste_obstacle= [(600,125),(500,235),(400,345),(110,565)]
    taille = [75,100]
    liste_obstacle= [(600,125),(200,125),(400,235),(110,345),(700,235),(100,565),(250,455),(700,455),(700,675), (350, 675),(400,15), (224, 785)]

    def collision_obstacle(x, y, obstacle):
        return obstacle[0] <= x <= obstacle[0] + taille[0] and obstacle[1] <= y <= obstacle[1] + taille[1]

    def collision(x, y, liste_obstacle):
        for o in liste_obstacle:
            if collision_obstacle(x, y, o):
                return True
        return False
    

    #BOUCLE INFINIE
    continuer = True
    while continuer:
        for evenement in pygame.event.get(): #Attente des événements
            if evenement.type == pygame.QUIT:
                continuer = False
            if evenement.type == pygame.KEYDOWN:
                if evenement.key == pygame.K_LEFT:
                    #On change les coordonnées du perso
                    if not collision(perso_x - 42 ,perso_y, liste_obstacle):
                        perso_x = perso_x - 42
                elif evenement.key == pygame.K_UP:
                    if not collision(perso_x ,perso_y - 110, liste_obstacle):
                        perso_y = perso_y - 110
                elif evenement.key == pygame.K_DOWN:
                    if not collision(perso_x ,perso_y + 110, liste_obstacle):        
                        perso_y = perso_y +110
                
                #Re-collage
                fenetre.blit(fond, (0,0))
                fenetre.blit(perso, (perso_x, perso_y))
                fenetre.blit(obs1, (obs1_x, obs1_y))
                fenetre.blit(obs2, (obs2_x, obs2_y))
                fenetre.blit(obs3, (obs3_x, obs3_y))
                fenetre.blit(obs4, (obs4_x, obs4_y))
                fenetre.blit(obs5, (obs5_x, obs5_y))
                fenetre.blit(obs6, (obs6_x, obs6_y))
                fenetre.blit(obs7, (obs7_x, obs7_y))
                fenetre.blit(obs8, (obs8_x, obs8_y))
                fenetre.blit(obs9, (obs9_x, obs9_y))
                fenetre.blit(obs10, (obs10_x, obs10_y))
                fenetre.blit(obs11, (obs11_x, obs11_y))
                fenetre.blit(obs12, (obs12_x, obs12_y))
                #Rafraichissement
                pygame.display.flip()

    

#programme principal     
                
continuer = True
while continuer:

    #FOND
    fond = pygame.image.load("olympique3.2.jpg").convert()
    fenetre.blit(fond, (0,0))

    #premier cadre "natation" où il faut cliquer pour arriver sur la page natation 
    cadre1 = pygame.image.load("rect_natation2.png").convert_alpha()
    cadre1.set_colorkey((0,0,0)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    fenetre.blit(cadre1, (350,200))

    #deuxième cadre "sauts d'obstacles" où il faut cliquer pour arriver sur la page de sauts 
    cadre2 = pygame.image.load("rect_sauts.png").convert_alpha()
    cadre2.set_colorkey((0,0,0)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    fenetre.blit(cadre2, (350,350))

    #troisième cadre "ice cross" où il faut cliquer pour arriver sur la page de ice cross
    cadre3 = pygame.image.load("rect_ice.png").convert_alpha()
    cadre3.set_colorkey((0,0,0)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
    fenetre.blit(cadre3, (350,500))

    #rafraichissement de la page 
    pygame.display.flip()
    
    #programme principal           
    for event in pygame.event.get():#attente des évènements 
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 1 :
                if est_natation(event.pos[0],event.pos[1]):#si on clique sur l'intervalle de pixel donné dans la fonction est_natation
                    page_natation()#alors la page natation s'ouvre 
                if est_sauts(event.pos[0], event.pos[1]):#si on clique sur l'intervalle de pixel donné dans la fonction est_sauts 
                    page_sauts()#alors la page sauts s'ouvre 
                if est_ice(event.pos[0], event.pos[1]):#si on clique sur l'intervalle de pixel donné dans la fonction est_ice
                    page_ice()#alors la page ice cross s'ouvre 
                    
                 
                    
                    
                    
                     


