import pygame
from pygame.locals import *

pygame.init()

fenetre = pygame.display.set_mode((1040, 880))

#FOND
fond = pygame.image.load("piscine.jpg").convert()
fenetre.blit(fond, (0,0))
pygame.display.flip()

#PERSO
perso = pygame.image.load("nageuse3.png").convert_alpha()
perso.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
perso_x = 750
perso_y = 75
fenetre.blit(perso, (perso_x, perso_y))

#obstacle
obstacle = pygame.image.load("bouee.png").convert_alpha()
obstacle.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle= fenetre.blit(obstacle,(350,365))

#obstacle2
obstacle2 = pygame.image.load("bouee.png").convert_alpha()
obstacle2.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle2= fenetre.blit(obstacle,(589,267))

#obstacle3
obstacle3 = pygame.image.load("bouee.png").convert_alpha()
obstacle3.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle3= fenetre.blit(obstacle3,(120,185))

#obstacle4
obstacle4 = pygame.image.load("bouee.png").convert_alpha()
obstacle4.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle4= fenetre.blit(obstacle4,(120,645))

#obstacle5
obstacle5 = pygame.image.load("bouee.png").convert_alpha()
obstacle5.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle5 = fenetre.blit(obstacle5,(678,458))

#obstacle6
obstacle6 = pygame.image.load("bouée3.png").convert_alpha()
obstacle6.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle6 = fenetre.blit(obstacle6,(675,720))

#obstacle7
obstacle7 = pygame.image.load("bouée3.png").convert_alpha()
obstacle7.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle7 = fenetre.blit(obstacle7,(487,65))

#obstacle8
obstacle8 = pygame.image.load("bouée3.png").convert_alpha()
obstacle8.set_colorkey((255,255,255)) #Rend le blanc (valeur RGB : 255,255,255) de l'image transparent
position_obstacle8 = fenetre.blit(obstacle8,(350,525))

#liste_obstacle= [(600,125),(500,235),(400,345),(110,565)]
liste_obstacle= [(350,365),(589,267),(120,185),(120,645),(678,458),(675,720),(487,65),(350,525)]

def collision_obstacle(x, y, obstacle):
    return obstacle[0] <= x <= obstacle[0] + taille[0] and obstacle[1] <= y <= obstacle[1] + taille[1]

def collision(x,y, liste_obstacle):
    for o in liste_obstacle:
        if collision_obstacle(x, y, o):
            return True
    return False

#Rafraîchissement de l'écran            
continuer = 1
#boucle infinie
continuer = True
while continuer:
    for evenement in pygame.event.get(): # Attente des événements
        if evenement.type == pygame.QUIT:
            continuer = False
        if evenement.type == pygame.KEYDOWN:
            if evenement.key == pygame.K_LEFT:
                perso_x -= 45
            elif evenement.key == pygame.K_UP:
                perso_y -= 98
            elif evenement.key == pygame.K_DOWN:
                perso_y += 98
    # Recollage
    fenetre.blit(fond, (0, 0))
    fenetre.blit(perso, (perso_x, perso_y))
    fenetre.blit(obstacle, position_obstacle)  # Affiche la première bouée
    fenetre.blit(obstacle2, position_obstacle2)  # Affiche la deuxième bouée
    fenetre.blit(obstacle3, position_obstacle3)  # Affiche la troisième bouée
    fenetre.blit(obstacle4, position_obstacle4)  # Affiche la quatrième bouée
    fenetre.blit(obstacle5, position_obstacle5)  # Affiche la cinquième bouée
    fenetre.blit(obstacle6, position_obstacle6) #Affiche la sixieme bouée
    fenetre.blit(obstacle7, position_obstacle7) #Affiche la septieme bouée
    fenetre.blit(obstacle8, position_obstacle8) #Affiche la huitième bouée
    
    
    # Rafraichissement    
    pygame.display.flip()
pygame.quit()